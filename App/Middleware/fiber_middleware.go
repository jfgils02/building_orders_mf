package Middleware

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
)

func FiberMiddleware(a *fiber.App) {
	a.Use(
		// Add CORS to each route.
		//Actualizar despues para que solo permita el sitio web
		cors.New(),
		// Add simple logger.
		logger.New(),
	)
}