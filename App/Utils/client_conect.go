package Utils

import data "gitlab.com/jfgils02/building_orders_mf/Database"

func ConnectClient(database string) {
	db := Decrypt(database)
	data.ConnectdbClient(db)
}
